<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors', '1');
/**
 * Class autoloader - loads classes automatically
 *
 * @param string $class_name - class name
 */
function __autoload($class_name) {
    @include ROOTURL."classes/".$class_name . '.php';
}

/**
 * Find server root
 *
 * @return string $rootaccess - root dir
 */
function findRoot()
{
	$times = substr_count($_SERVER['PHP_SELF'],"/");
	$rootaccess = "";
	$i = 2;
	while ($i < $times)
	{
		$rootaccess .= "../";
		$i++;
	}
	
	return $rootaccess;
}
$root = findRoot();
// Set site constants
define('ROOTURL', $root);
define("PAYPALMAIL","kusdevelopers@gmail.com");

/**
 * Check Client Login
 *
 */
function checkLogin(){				
	if (isset($_SESSION['lastlogin']) && (time() - $_SESSION['lastlogin'] > 900)) {
		// last request was more than 15 minutes ago
		session_destroy();
		header('Location: http://kusdemos.com/slicedbread/');
		die();
	}
}
?>