<?php include("includes/common.php"); 
	$loginObj = new LoginClass(); 
	if($_POST)
	{
		//check if its an ajax request, exit if not
		if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {        
			$output = json_encode(array( //create JSON data
				'type'=>'error', 
				'text' => 'Sorry Request must be Ajax POST'
			));
			die($output); //exit script outputting json data
		} 
		
		//Sanitize input data using PHP filter_var().
		$username     = filter_var($_POST["username"], FILTER_SANITIZE_STRING);
		$password     = filter_var($_POST["password"], FILTER_SANITIZE_STRING);
		
		$response = $loginObj -> clientLogin($username,$password);	
		die($response);
	}
?>
