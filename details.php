<?php include("includes/common.php"); 
	  checkLogin();
	  $projectObj = new ProjectClass();
	  $prjId = $_REQUEST['id'];
	  $projectInfo = $projectObj -> getProject($prjId);
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Slicedbread - Project Details</title>
<link href="<?php echo SITEURL; ?>css/style.css" rel="stylesheet" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>
<script src="<?php echo SITEURL; ?>js/jquery.min.js"></script>
</head>

<body class="d-bg-color">
<header>
  <div class="wrapper">
    <div class="logo"> <a href="<?php echo SITEURL; ?>"><img src="<?php echo SITEURL; ?>image/logo.png" alt="logo" /></a> </div>
    <div class="breadcrumb">
      <ul>
        <li><a href="<?php echo SITEURL.'client-portal'; ?>">Dashboard  </a> > </li>
        <li><?php echo $projectInfo['title']; ?></li>
      </ul>
    </div>
  </div>
</header>
<div class="container">
  <div class="wrapper">
    <div class="d-heading">
      <h2><?php echo $projectInfo['title']; ?></h2>
    </div>
    <div class="project-details">
      <?php echo $projectInfo['description']; ?>
    </div>
  </div>
</div>
<footer> </footer>
</body>
</html>