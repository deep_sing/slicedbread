<?php include("includes/common.php"); 
	  checkLogin();
	  $projectObj = new ProjectClass();
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Slicedbread - Client Dashboard</title>
<link href="<?php echo SITEURL; ?>css/style.css" rel="stylesheet" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>
<script src="<?php echo SITEURL; ?>js/jquery.min.js"></script>
</head>

<body class="d-bg-color">
<header>
  <div class="wrapper">
    <div class="logo"> <a href="<?php echo SITEURL; ?>"><img src="<?php echo SITEURL; ?>image/logo.png" alt="logo" /></a> </div>    
  </div>
</header>
<div class="container">
  <div class="wrapper">
    <div class="d-heading">
      <h2>Dashboard</h2>
    </div>
    <div class="d-categories">
      <ol type="1">
        <?php $prjlist = $projectObj -> getAllProject();
		  	foreach($prjlist as $prjrow){
				echo '<li><a href="'.SITEURL.'client-portal/'.$prjrow['seo_url'].'">'.$prjrow['title'].'</a></li>';
			}
		  ?>
      </ol>
    </div>
  </div>
</div>
<footer> </footer>
</body>
</html>