<?php

class ProjectClass{
	private $db;
	
	/**
     * Constructor to create instance of DB object
     *
	 */
	public function __construct(){
		$this -> db = DbClass::getInstance();
		$this -> db -> getsettingsData();
	}	
	/**
     * Create url friendly string
     *
	 * @return string - url string
	 */
	public function toAscii($str, $delimiter='-') {
		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
		$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
		$clean = strtolower(trim($clean, '-'));
		$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
	
		return $clean;
	}
	
	
	/**
     * Get project
     *
	 * @param int - project id
	 *
	 */
	public function getProject($pId){
		$pslug = $this -> db -> cleanData($pId);
		$return_array =  $this -> db -> row("SELECT * FROM project WHERE seo_url = :pslug",array("pslug"=>$pslug));
		return $return_array;
	}
		
		
	/**
     * Get all project
     *
	 *
	 */
    public function getAllProject(){		
		$results =  $this -> db -> query("SELECT * FROM project WHERE 1 AND status = :st",array("st"=>"Y"));
        return $results;
    }	
}	
?>